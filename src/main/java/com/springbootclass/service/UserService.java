package com.springbootclass.service;

import com.springbootclass.model.User;

import java.util.List;

public interface UserService {
    User create(User user);
    List<User> getUsers();
    User getUserByEmail(String  email);
    User updateUserInfo(Long id, User user);

    void deleteUser(Long id);
}
