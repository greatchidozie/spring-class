package com.springbootclass.service;

import com.springbootclass.model.User;
import com.springbootclass.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepo userRepo;

    @Override
    public User create(User user) {
        User newUser = new User();
        User foundUser = userRepo.findByEmail(user.getEmail());
        if (foundUser.getName().equals(user.getEmail())){
            System.out.println("User with email already exist");
        }
        newUser.setEmail(user.getEmail());
        newUser.setPassword(user.getPassword());
        newUser.setName(user.getName());
        newUser.setPhone(user.getPhone());
        return userRepo.save(newUser);
    }

    @Override
    public List<User> getUsers() {
        return userRepo.findAll();
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    @Override
    public User updateUserInfo(Long id, User user) {
        User userToUpdate = findUserById(id);
        userToUpdate.setPhone(user.getPhone());
        userToUpdate.setName(user.getName());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setEmail(user.getEmail());
        return userRepo.save(userToUpdate);
    }

    private User findUserById(Long id){
        if (userRepo.findById(id).isEmpty()){
            System.out.println("User with Id does not exist");
        }
        return userRepo.findById(id).get();
    }

    @Override
    public void deleteUser(Long id) {
        Optional<User> userToDelete =userRepo.findById(id);
        userRepo.delete(userToDelete.get());
    }
}
